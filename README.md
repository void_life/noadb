# NoADB -- Always disable adb

**中文版请阅读README_cn.md**

## Introduction

ADB(Android Debug Bridge) is a versatile command-line tool that allows you communicate with a device. On your phone, there's a service called "adbd"(adb daemon) which can communicate with computer though USB cable or wireless connection.

This tool enables developers to install, debug applications easily. However, it can also be used to:

- Install and start spy softwares
- Read almost all data from your phone 
- Record screen activity or take screenshots

From Android 4.2.2, the system shows a dialog asking whether to accept an RSA key that allows debugging through this computer. But government may force users to unlock the phone and accept their RSA key. This application can disable ADB daemon, so nobody can access adb (even if they force phone user to activate adb debugging)

## How to Install

Flash this zip, reboot your phone.

You can verify if this module works or not by connecting your phone to a computer and enable adb debugging. Then enter `adb devices`  on your computer, you will get:

```
root:~$ adb devices
List of devices attached

```

## How to disable this module (reactivate adb)

When you really need adb back, you can deactivate this module and reboot.

## Troubleshoot

- Android System: "USB debugging connected"

  This module does not deactivate adb from developer mode. Instead, it disabled it from a shell process. Although you can see this message, nobody can use adb debugging because the service that listen to adb connect request is disabled.

- Unable to transfer files to computer / Android System: "USB connected" after disconnected

  This is a known bug. A workaround is reboot your phone and everything will be fine. You can also deactivate this module and only use it when you need it.
  
  Actually, enabling this module will disable MTP and file sharing protocol, so it is actually a "Feature", and I'm not going to fix it.